import { CommandInteraction, SlashCommandBuilder } from 'discord.js'
import { Command } from './bot'
import { exec } from 'node:child_process'
import { writeFile } from 'node:fs/promises'

const command: Command = {
  name: 'spoils',
  data: new SlashCommandBuilder()
    .setName('spoils')
    .setDescription('Get Spoils of Conquest from a f2p Destiny 2 raid')
    .addStringOption(option =>
      option.setName('raid')
            .setDescription('The activity to load')
            .setRequired(true)
            .addChoices(
              { name: 'Vault of Glass (4 chests)', value: 'vog' },
              { name: 'King\'s Fall (1 chest)', value: 'kf' },
              { name: 'Crota\'s End (1 chest)', value: 'ce' }
            )
    )
    .addStringOption(option =>
      option.setName('username')
            .setDescription('Your Bungie username. ex: username#1234')
            .setRequired(true)
    )
    .toJSON(),
  execute: async (interaction: CommandInteraction) => {
    const raid = interaction.options.get('raid', true).value
    const username = interaction.options.get('username', true).value

    if (!raid) {
      return await interaction.reply({ content: 'Could not verify the raid.', ephemeral: true })
    }

    if (!username) {
      return await interaction.reply({ content: 'Could not get the username.', ephemeral: true })
    }

    const splitUsername = username.toString().split('#')

    if (splitUsername.length !== 2 || parseInt(splitUsername[1], 10).toString() !== splitUsername[1]) {
      return await interaction.reply({ content: 'The username is not valid.', ephemeral: true })
    }

    const config = `[Config]
CharacterIndex=1
PlanetImagePath=images/legends.png
ActivityImagePath=images/${raid.toString()}.png
PlayerName=${splitUsername[0]}
PlayerCode=${splitUsername[1]}`
    await writeFile('./config.txt', config)
    exec('AutoHotkey.exe ./loadActivity.ahk')

    return await interaction.reply({ content: 'Inviting you right now!', ephemeral: true })
  }
}

export default command
