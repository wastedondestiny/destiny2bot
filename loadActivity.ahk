WAIT_FOR_CHECK_INTERVAL=100 ; check every x seconds
WAIT_FOR_MAX_CHECK_LOOPS=700  ; maximum count of checks

; Load the configuration
ConfigFile := "config.txt"
if !FileExist(ConfigFile) {
  MsgBox, Configuration file not found!
  ExitApp
}

IniRead, CharacterIndex, %ConfigFile%, Config, CharacterIndex
IniRead, PlanetImagePath, %ConfigFile%, Config, PlanetImagePath
IniRead, ActivityImagePath, %ConfigFile%, Config, ActivityImagePath
IniRead, PlayerName, %ConfigFile%, Config, PlayerName
IniRead, PlayerCode, %ConfigFile%, Config, PlayerCode

#Include Gdip.ahk
GetImageSize(ImageFullPath, byref W, byref H) {
  GDIPToken := Gdip_Startup() 
  pBM := Gdip_CreateBitmapFromFile( ImageFullPath ) 
  W:= Gdip_GetImageWidth( pBM ), H:= Gdip_GetImageHeight( pBM ) 
  Gdip_DisposeImage( pBM ), Gdip_Shutdown( GDIPToken )
}

WaitForImage(filenameImage) {
  global WAIT_FOR_CHECK_INTERVAL
  global WAIT_FOR_MAX_CHECK_LOOPS
  
  loopCount = 0;
  
  Loop {
  	Sleep, %WAIT_FOR_CHECK_INTERVAL%
  	ImageSearch, FoundX, FoundY, 0, 0, A_ScreenWidth, A_ScreenHeight, *100 %filenameImage%
  	if (ErrorLevel = 0)
  		break ; image found
  	loopCount += 1
  	
  	if (loopCount > WAIT_FOR_MAX_CHECK_LOOPS) {
  		Msgbox Image %filenameImage% not found!
  		exitApp
  	}
  }
}

; Function to click on an image
ClickImage(ImagePath, Timeout := 5000) {
  CoordMode, Pixel, Screen
  ImageSearch, x, y, 0, 0, A_ScreenWidth, A_ScreenHeight, *100 %ImagePath%
  GetImageSize(ImagePath, W, H)

  if (ErrorLevel = 0) {
    Click, %x% + %W%/2, %y% + %H%/2
    return true
  } else {
    return false
  }
}

; Start of the script
; Wait for Destiny 2 to be active
WinWaitActive, ahk_exe Destiny2.exe

; Select the character
Sleep, 2000
MouseClick, left, 1300, 450 + (CharacterIndex - 1) * 115 ; Adjust coordinates

; Open the map
Sleep, 5000
Send, {M}
Sleep, 2000

; Click on the desired planet
if !ClickImage(PlanetImagePath) {
  MsgBox, Planet image not found!
  ExitApp
}
Sleep, 2000

; Click on the desired activity
if !ClickImage(ActivityImagePath) {
  MsgBox, Activity image not found!
  ExitApp
}
Sleep, 2000

; Invite the player
Send, {Enter}
Sleep, 500
Send, /invite %PlayerName%{#}%PlayerCode%
Sleep, 500
Send, {Enter}
Sleep, 2000

; Check a portion of the screen to confirm player joined
; This requires an image of the player's name in the roster
; Assuming the image is named PlayerJoined.png
WaitForImage("images/joined.png")

; Click on the "launch" button
; Assuming coordinates for the "launch" button
MouseClick, left, 1630, 900 ; Adjust coordinates for the center of the screen
; Wait for the activity to start
Sleep, 15000
; Quit the activity
Send, {Tab}
Send, {Escape}
Send, {Escape}
Sleep, 1000
Click, 700, 600 ; Adjust coordinates for "Change Character"

MsgBox, Script completed!
ExitApp