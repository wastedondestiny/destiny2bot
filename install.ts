import { Service } from 'node-windows'

// Create a new service object
const svc = new Service({
  name: 'BinarBot',
  description: 'Destiny 2 automation',
  script: `cd ${__dirname} && npm run start`
})

svc.on('install', function() {
  svc.start()
})

svc.install()
