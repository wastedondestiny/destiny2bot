import dotenv from 'dotenv'
dotenv.config()

import { Client, PermissionsBitField, IntentsBitField, Collection, CommandInteraction, InteractionResponse, RESTPostAPIChatInputApplicationCommandsJSONBody, Routes } from 'discord.js'
import { REST } from '@discordjs/rest'
import spoilsCommand from './spoilsCommand'

export type Command = {
  name: string,
  data: RESTPostAPIChatInputApplicationCommandsJSONBody,
  execute: (interaction: CommandInteraction) => Promise<InteractionResponse<boolean> | undefined>
}

const permissions = new PermissionsBitField()
permissions.add(PermissionsBitField.Flags.ViewChannel)

const intents = new IntentsBitField()

const commands = new Collection<string, Command>()
commands.set(spoilsCommand.name, spoilsCommand)

const client = new Client({ intents })
client.on('interactionCreate', async interaction => {
  if (!interaction.isChatInputCommand()) return
  const command = commands.get(interaction.commandName)
  if (!command) return

  try {
    await command.execute(interaction)
  } catch (error: Error|unknown) {
    console.error(error)
  }
})
client.login(process.env.BOT_TOKEN)

const rest = new REST({ version: '10' }).setToken(process.env.BOT_TOKEN || '')
rest.put(Routes.applicationCommands(process.env.BOT_CLIENT_ID ||''), {
  body: commands.map(x => x.data)
})

export default client
export const invite = `Invite the bot: https://discord.com/oauth2/authorize?client_id=${process.env.BOT_CLIENT_ID}&scope=bot%20applications.commands&permissions=${permissions.bitfield}`
