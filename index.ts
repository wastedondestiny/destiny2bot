import dotenv from 'dotenv'
dotenv.config()

import discordBot, { invite as discordBotInvite } from './bot'
import { exec, execSync } from 'node:child_process'

discordBot.on('ready', async () => {
  try {
    execSync('./legendary.exe update Destiny2')
    exec('./legendary launch Destiny2')
  } catch (_) {}

  console.log(discordBotInvite)
})
